
#include <iostream>

using namespace std;


class Animal
{
private:

public:
	virtual void voice()
	{
		cout << "sound";
	}

};


class Dog : public Animal
{
private:

public:
	void voice() override
	{
		cout << "Woof";
	}
};


class Cat : public Animal
{
private:

public:
	void voice() override
	{
		cout << "Meow";
	}
};


class Cow : public Animal
{
private:

public:
	void voice() override
	{
		cout << "Moo";
	}
};


class Bird : public Animal
{
private:

public:
	void voice() override
	{
		cout << "Chik-Chirik";
	}
};



int main()
{

	Animal* D = new Dog();
	Animal* C = new Cat();
	Animal* Co = new Cow();
	Animal* B = new Bird();

	Animal* Array[] = { D, C, Co, B };

	for (int i = 0; i < 4; i++)
	{
		Array[i]->voice();
		cout << "\n";
	}
	



	

}
